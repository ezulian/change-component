;;;; change-component.asd

(asdf:defsystem #:change-component
  :description "Change the component of a kernel bug"
  :author "Wade Mealing <wmealing@redhat.com>"
  :license  "MIT"
  :version "0.0.1"
  :depends-on (#:cl-jira
               #:hunchentoot #:cl-template #:str
               #:cl-who #:easy-routes #:alexandria
               #:cl-store #:spinneret
               )
  :serial t
  :entry-point "change-component::main"
  :components ((:file "package")
               (:file "change-component-httpd")
               (:file "change-component")))
