
;;;; change-component.lisp

(in-package #:change-component)

(defun find-package-matching-cves-in-jira (package cve-number)
  "Find all matching cves in jira of numpber "
  (let ((query-string (str:concat "summary ~ \"" cve-number " " package ":\" AND project = 'RHEL' ")))
    (getf (cl-jira:query-with-raw query-string) :|issues|)))

(defun find-all-matching-cves-in-jira (cve-string)
  "Find all matching cves in jira of numpber "
  (let ((query-string (str:concat
                       "summary ~ \"" cve-string  "\" AND project = RHEL")))
    (format t "DEBUG: QUERY STRING: ~a~%" query-string)
    (getf (cl-jira:query-with-raw query-string) :|issues|)))

(defun process (cve-number new-component)

  (format t "Changing component for CVE: ~s~%" cve-number )
  (format t "Changing to component: ~s~%" new-component )

  (let* ((jira-issues (find-package-matching-cves-in-jira "kernel" cve-number)))
    (loop for issue in jira-issues
          do (progn
               (format t "Setting issue ~s to component: ~s ~%"
                       (getf issue :|key|)
                       new-component)

               (cl-jira:set-component (getf issue :|key|)
                                      :component (str:concat "kernel " new-component)))))


  (let* ((jira-issues (find-package-matching-cves-in-jira "kernel-rt" cve-number)))
    (loop for issue in jira-issues
          do (progn
               (format t "Setting issue ~s to component: ~s ~%"
                       (getf issue :|key|)
                       new-component)
               (cl-jira:set-component (getf issue :|key|)
                                      :component (str:concat "kernel-rt " new-component))))))
