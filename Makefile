# SBCL := /home/wmealing/sbcl/bin/sbcl
SBCL := `which sbcl` 


all:
	sbcl --load change-component.asd --eval '(ql:quickload :change-component)' --eval "(sb-ext:save-lisp-and-die #p\"change-component\"    :toplevel #'change-component:change-component-main :executable t)"

sync:
	rm -rf change-component
	rsync -avz ./  wmealing@dell-kbuilder-01.lab.eng.rdu2.redhat.com:~/quicklisp/local-projects/change-component/
