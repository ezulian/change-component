
;;;; change-component-server.lisp

(in-package #:change-component)

(defvar *server* nil
  "Server instance (Hunchentoot acceptor).")

(defvar *port* 4243
  "Server port (Hunchentoot acceptor)")

(defvar *all-kernel-components* 
  '((:name "Components are being refreshed (reload this page in ~10 seconds)")))

(defvar *template-dir*
  (asdf:system-relative-pathname "change-component" "templates/"))

(push (hunchentoot:create-folder-dispatcher-and-handler
       "/static/"
       (truename (asdf:system-relative-pathname "change-component" "static")))
      hunchentoot:*dispatch-table*)

(defun apply-template (filename values)
  (with-open-file (template-file (merge-pathnames *template-dir* filename))
                  (let ((template (make-string (file-length template-file))))
                    (read-sequence template template-file)
                    (funcall (cl-template:compile-template template) values))))

(defun summary-matches-kernel-or-rt (item)
  (let ((summary (getf item :summary)))

    (if (or
         (and
          (str:containsp "CVE-" summary)
          (str:containsp "kernel:" summary))
         (and
          (str:containsp "CVE-" summary)
          (str:containsp "kernel-rt:" summary))
         )
        "checked"
      "unchecked"
      )))

(defun @has-jira-key-cookie (next)
  (let ((*cookie* (hunchentoot:cookie-in "jira-key")))
    (if (not *cookie*)
        (hunchentoot:redirect "/instructions/")
      (funcall next))))

(defun @set-cookie-in-cl-jira (next)
  (let ((*cookie* (hunchentoot:cookie-in "jira-key")))
    (progn
      (cl-jira:set-api-key *cookie*)
      (funcall next))))

(easy-routes:defroute index-get ("/" :method :get) ()
  (apply-template "index.html" (list :components *all-kernel-components*)))

(easy-routes:defroute instructions-get ("/instructions" :method :get) ()
  (apply-template "instructions.html" (list :components '())))

(defun fetch-issue-fields (issue-id)
  (getf (cl-jira:get-issue issue-id ) :|fields|))

(defun fetch-issue-formatted (issue-id)
  (format t "Fetching issue ~s ~%"  issue-id)
  (list :issue-id issue-id :summary (getf (fetch-issue-fields issue-id) :|summary|) ))

(defun process-checkbox-post-data (post-data)
  (mapcar #'car
          (remove-if (lambda (p) (string= "subcomponent" (car p) )) post-data)))

(defun parse-issue-key (issue-data)
  (getf issue-data :|key|))

(easy-routes:defroute search-post ("/search/" :method :post
                                              :decorators (@has-jira-key-cookie @set-cookie-in-cl-jira)) ()

  (let* ((kernel-cve-matches (find-all-matching-cves-in-jira (hunchentoot:post-parameter "search-value")))
         (just-id-list (mapcar 'parse-issue-key kernel-cve-matches))
         (matched-issues (mapcar 'fetch-issue-formatted just-id-list)))

    (format t "KERNEL CVE MATCHES: ~s~%" kernel-cve-matches)
    (format t "JUST ID LIST: ~s~%" just-id-list)
    (format t "MATCHED ISSUES: ~s~%" matched-issues)

    (apply-template "search.html" (list :cve-matches matched-issues))))

(easy-routes:defroute process-post ("/process/" :method :post
                                                :decorators (@has-jira-key-cookie @set-cookie-in-cl-jira)) ()
  (let* ((post-parameters (hunchentoot:post-parameters*))
         (subcomponent (hunchentoot:post-parameter "subcomponent"))
         (issues-to-process (process-checkbox-post-data post-parameters))
         (result (set-jira-for-issues issues-to-process subcomponent)))
    (format t "PROCESS-POST RESULT: ~s~% " result )
    (apply-template "process-results.html" (list :results result))
    ))

(easy-routes:defroute set-client-cookie-request ("/setcookie/" :method :get) ()
  (let* ((jira-key (hunchentoot:post-parameter "jira-key")))
    (format t "SET-CLIENT-COOKIE: ~s~% " jira-key )
    (if jira-key
        (apply-template "set-cookie-request.html" (list :jira-key jira-key))
        (apply-template "set-cookie-request.html" (list :jira-key "")))))

(easy-routes:defroute set-client-cookie-response ("/setcookie/" :method :post) ()
  (let* ((jira-key (hunchentoot:post-parameter "jira-key")))
    (progn
      (format t "SET-CLIENT-COOKIE: ~s~% " jira-key )
      (apply-template "set-cookie-response.html" (list :jira-key jira-key)))))

(defun get-package-from-summary (issue-id)
  (let ((summary (getf (fetch-issue-formatted issue-id) :summary)))
    (cond
      ((str:containsp "kernel-rt" summary) "kernel-rt")
      ((str:containsp "kernel" summary) "kernel")
      (t "unknown")
      )))

(defun set-jira-for-issues (issue-list subcomponent)

  (loop for issue-id in issue-list
        collect (progn
                  (let* ((package (get-package-from-summary issue-id))
                         (full-component (str:concat package " " subcomponent)))

                    (if (string= package "unknown")
                        '(:issue-id issue-id
                          :action "none"
                          :result "Not changed, not a kernel or kernel-rt issue") ;; dont touch it.
                        (execute-fix-jira issue-id full-component) ;; change it.
                        )))))

(defun remove-kernel-prefix (single-jira-component)
  (let ((name (getf single-jira-component :|name|)))
    (str:replace-all "kernel / " "" name)))

(defun format-components (jira-data)
  (mapcar (lambda (e) (list :name (remove-kernel-prefix e) ) ) jira-data))

(defun build-component-list ()
  (let* ((all-components (cl-jira:get-project-components "RHEL"))
         (kernel-components-raw (get-kernel-components-for-rhel all-components))
         (kernel-components-formatted (format-components kernel-components-raw)))
    (setq *all-kernel-components* kernel-components-formatted)))

;; (cl-jira:get-project-components "RHEL")
(defun get-kernel-components-for-rhel (all-components)
  (remove-if-not (lambda (c)
                   (if (str:containsp "kernel /" (getf c :|name|))
                       t
                       nil)) all-components))

(defun execute-fix-jira (issue-id component)
  (declare (ignore component))
  ;; (cl-jira:set-component issue :component component)
  ;; return something like this on success.
  `(:issue-id ,issue-id
              :action "Changed"
              :result "New Component set!"))

(defun start-server (&key (port *port*))
  (format t "~&Starting the web server on port ~a" port)
  (force-output)
  (setf *server* (make-instance 'easy-routes:easy-routes-acceptor
                                :port (or port *port*)
                                :document-root nil))
  (hunchentoot:start *server*))

(defun stop-server ()
  (hunchentoot:stop *server*))

(defun change-component-main ()
  (start-server)
  (build-component-list)
  (sleep most-positive-fixnum))

